package com.company;

import com.company.model.Bank;
import com.company.model.BlackMarket;
import com.company.model.Cantor;
import com.company.model.MoneyHouse;

/**
 * Created by Dimon on 08.04.18.
 */
public class Generator {

    public Generator() {
    }


    public static MoneyHouse[] generateMoneyHouses() {
        MoneyHouse[] moneyHouses = new MoneyHouse[9];

        String[] bankNames = {"PB", "OB", "PUMB"};
        float[] bankCources = {26f, 25, 28};
        int bankLimit = 150000;

        String[] cantorNames = {"Blago", "Zolotoy", "Nadezhnost"};
        float[] cantorCources = {27.3f, 26.7f, 25.5f};
        int[] cantorLimits = {500, 800, 1000};

        String[] blackMarketNames = {"Ramil", "Mustafa", "Vova"};
        float[] blackMarketCources = {25.8f, 25.9f, 25.5f};
        int blackMarketLimit = Integer.MAX_VALUE;

        moneyHouses[0] = new Bank(bankNames[0], bankCources[0], bankLimit);
        moneyHouses[1] = new Bank(bankNames[1], bankCources[1], bankLimit);
        moneyHouses[2] = new Bank(bankNames[2], bankCources[2], bankLimit);
        moneyHouses[3] = new Cantor(cantorNames[0], cantorCources[0], cantorLimits[0]);
        moneyHouses[4] = new Cantor(cantorNames[1], cantorCources[1], cantorLimits[1]);
        moneyHouses[5] = new Cantor(cantorNames[2], cantorCources[2], cantorLimits[2]);
        moneyHouses[6] = new BlackMarket(blackMarketNames[0], blackMarketCources[0], blackMarketLimit);
        moneyHouses[7] = new BlackMarket(blackMarketNames[1], blackMarketCources[1], blackMarketLimit);
        moneyHouses[8] = new BlackMarket(blackMarketNames[2], blackMarketCources[2], blackMarketLimit);//can't did it with cycles


        return moneyHouses;
    }


}
