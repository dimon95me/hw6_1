package com.company.model;

/**
 * Created by Dimon on 07.04.18.
 */
public class MoneyHouse {
    protected String name;
    protected float course;
    protected int limit;

    public MoneyHouse(String name, float course, int limit) {
        this.name = name;
        this.course = course;
        this.limit = limit;
    }

    public float exchange(int count) {
        return (float) (count * course);
    }

    public boolean verify(int count) {
        return count <= limit;
    }

    public void print() {
        System.out.println("----------");
        System.out.println(name);
        System.out.println(String.format("Course %s", course));
    }
}
