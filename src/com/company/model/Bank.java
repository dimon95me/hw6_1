package com.company.model;

/**
 * Created by Student on 03.04.2018.
 */
public class Bank extends MoneyHouse {

    public Bank(String bankName, float bankCource, int bankLimit) {
        super(bankName,bankCource,bankLimit);
    }

    public float exchange(int count) {
        return (float) (count * course * 0.95f);
    }
}
