package com.company;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner in = new Scanner(System.in);

        System.out.println("Enter your money count:");
        int count = Integer.parseInt(in.nextLine());

        StockExchange library = new StockExchange(Generator.generateMoneyHouses());

        library.printIfCount(count);
        //library.printAll();

    }
}
