package com.company;

import com.company.model.MoneyHouse;

/**
 * Created by Dimon on 08.04.18.
 */
public class StockExchange {

    private MoneyHouse[] moneyHouses;

    public StockExchange(MoneyHouse[] moneyHouses) {
        this.moneyHouses = moneyHouses;
    }

    public void printAll() {
        for (MoneyHouse moneyHouse : moneyHouses) {
            moneyHouse.print();
        }
    }

    public void printIfCount(int count) {
        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse.verify(count)) {
                moneyHouse.print();
                System.out.println(moneyHouse.exchange(count));
            }
        }
    }
}